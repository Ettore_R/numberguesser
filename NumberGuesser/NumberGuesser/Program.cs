﻿using System;

namespace NumberGuesser
{
    class Program
    {
        static void Main(string[] args)
        {
            RenderAppInfo(); // Display app information to console

            GreetUser(); // Greet user and get name

            while (true) {
                // init correct random number
                int correctNumber = new Random().Next(1, 10);

                // init guess varible
                int guess = 0;

                // Ask user for input 
                Console.WriteLine("Guess a number between 1 and 10");

                while (guess != correctNumber)
                {
                    // get user input
                    string input = Console.ReadLine();

                    if (!int.TryParse(input, out guess))
                    {
                        PrintColorMessage("That is not a number, please enter a number", ConsoleColor.Red);

                        continue;
                    }

                    // convert string to INT and store in guess
                    guess = Int32.Parse(input);

                    // Match guess to correct number
                    if (guess != correctNumber)
                    {
                        PrintColorMessage("Wrong, please try again", ConsoleColor.Red);
                    }
                }


                // Number is correct, ask user to play again
                PrintColorMessage("Correct! You won! Play again? (y/n)", ConsoleColor.Yellow);

                string playAgain = Console.ReadLine().ToUpper();

                if (playAgain == "Y")
                {

                    continue;

                } else if (playAgain == "N") {

                    return;

                }
            }
        }

        static void RenderAppInfo()
        {
            //App varibles
            string appName = "Number Guesser";
            string appVersion = "1.0.0";
            string appAuthor = "Ettore Antonio 'The One' Raimondi";

            // Change text color
            Console.ForegroundColor = ConsoleColor.Green;

            // Display app Info
            Console.WriteLine("{0}, Version: {1}, Author: {2}", appName, appVersion, appAuthor);

            // Reset console color
            Console.ResetColor();
        }

        static void GreetUser()
        {
            // Ask for users name
            Console.WriteLine("What is your name?");

            // Store user input
            string inputName = Console.ReadLine();

            // Greet user
            Console.WriteLine("Hello {0}, lets play a game...", inputName);
        }

        static void PrintColorMessage (string message, ConsoleColor color)
        {
            // Change console color
            Console.ForegroundColor = color;

            // Display message
            Console.WriteLine(message);

            // Reset console color
            Console.ResetColor();
        }
    }
}
